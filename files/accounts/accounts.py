# -*- coding: utf-8 -*-

INIS = """
config1.ini
"""

def inis():
    return INIS.split()

def read(ini):
    import configparser
    config = configparser.RawConfigParser()
    config.read("accounts/" + ini)
    return config