# AltBot

An altcoin Python3 bot that detects Bittrex coin surges and automates trades.

# Getting Started

## Methodology

AltBot uses an algorithm to determine the highest growing coin in the last hour.
The bot buys the selected altcoin at market value, and places a sell order.

# Prerequisites

Install Python3 and required plugins

```
    sudo apt-get install git gcc python3 python3-pip -y
```

Install python-bittrex (https://gitlab.com/chadporter/python-bittrex)

## Deployment

```
    sudo pip3 install -r requirements.txt
```

## AltBot Configuration
 
1. Configure the system.ini file as documented. `sudo vim files/system.ini`
2. Configure config1.ini file as documented in `files/accounts/ini-0-sample.ini`. (takeprofit = 5 works best, change deposit amount = 1)

```
    sudo cp files/ini-0-sample.ini /files/config1.ini
```

## Cronjob

Verify that python-invoke is being run in the /usr/bin directory, and setup the cronjob.
```
    which invoke
    contab -e
```

```
    INVOKE=/usr/local/bin/invoke

    00   * * * * cd ~/altbot/files/ ; $INVOKE pull ; $INVOKE buy
    */1  * * * * cd ~/altbot/files/ ; $INVOKE sell
    @weekly      cd ~/prg/surgetrader/src/ ; $INVOKE clearprofit
    @weekly      cd ~/prg/surgetrader/src/ ; $INVOKE cancelsells
    11   0 * * * cd ~/altbot/files/ ; $INVOKE report -d yesterday
    22   0 1 * * cd ~/altbot/files/ ; $INVOKE report -d lastmonth
    
```
#Note

`clearprofit` will re-open trades older than 28 days.

## Using without cronjob

Although not recommended, AltBot can be run manually from the `files` directory.

```
    cd ~/altbot/files
    invoke pull
    invoke buy
    invoke sell
    invoke report -d yesterday
    invoke report -d lastmonth
```

# License

This project is licensed under the GNU GPL License - see the LICENSE file for details